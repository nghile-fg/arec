---json
{
    "title": "あいち不動産コンサルタンツ",
    "description": "土地、中古住宅、新築住宅、中古マンションなどの不動産売買仲介情報。 不動産有効活用、各種権利調整、相続対策関連、不動産経営分野などの不動産コンサルティング。 
  ライフプラン作成、住宅ローンアドバイス、資金計画相談など資産運用の不動産FP相談。 賃貸マンション、貸家、テナント（貸し店舗・貸し事務所）などの賃貸仲介情報",
    "keywords":"あいち不動産コンサルタンツ,不動産コンサルティング,FP,ファイナンシャルプランナー, 不動産, 三河, 刈谷, 安城, 知立, 賃貸, 物件, 仲介, 売買, 土地, 貸店舗, 貸事務所, 
  売り地, 中古, マンション",
    "slug": "",
    "layout": "contact.html",
    "tag": [],
    "date": "2017-04-18T04:38:58.000Z",
    "publishDate": "2017-04-18T04:38:58.000Z",
    "draft": false,
    "__content__": ""
}
---
<div class="p-contact">
    <!--************************ MainBlock start ************************-->
    <div class="c-mainBlock">
        <div class="c-mainBlock__img"><img src="./assets/img/contact/index_contact.png" alt="会社案内"></div>
    </div>
    <!--************************ MainBlock end ************************-->
    <ul class="c-navi1">
        <li><a href="/contact_all.html">各種お問い合わせ</a></li>
        <li><a href="/reservation.html">来店予約</a></li>
    </ul>
    <div class="p-contact-thank">
        <h3>お問い合わせありがとうございます。</h3>
        <p>また､お客様のもとに現在 自動返信メールを送信させて頂きました｡ <br>
            万が一､メールが届いていない場合は､ご入力頂いたメールアドレスが<br>
            間違っている可能性がございます｡<br>
            お手数ですが､もう一度お問い合わせフォームよりご入力下さい｡</p>
    </div>
</div>
