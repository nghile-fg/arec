---json
{
    "title": "あいち不動産コンサルタンツ",
    "description": "土地、中古住宅、新築住宅、中古マンションなどの不動産売買仲介情報。 不動産有効活用、各種権利調整、相続対策関連、不動産経営分野などの不動産コンサルティング。 
  ライフプラン作成、住宅ローンアドバイス、資金計画相談など資産運用の不動産FP相談。 賃貸マンション、貸家、テナント（貸し店舗・貸し事務所）などの賃貸仲介情報",
    "keywords":"あいち不動産コンサルタンツ,不動産コンサルティング,FP,ファイナンシャルプランナー, 不動産, 三河, 刈谷, 安城, 知立, 賃貸, 物件, 仲介, 売買, 土地, 貸店舗, 貸事務所, 
  売り地, 中古, マンション",
    "slug": "",
    "layout": "contact.html",
    "tag": [],
    "date": "2017-04-18T04:38:58.000Z",
    "publishDate": "2017-04-18T04:38:58.000Z",
    "draft": false,
    "__content__": ""
}
---
<div class="p-contact">
    <!--************************ MainBlock start ************************-->
    <div class="c-mainBlock">
        <div class="c-mainBlock__img"><img src="./assets/img/contact/index_contact.png" alt="会社案内"></div>
    </div>
    <!--************************ MainBlock end ************************-->
    <ul class="c-navi1">
        <li><a href="/contact_all.html">各種お問い合わせ</a></li>
        <li><a href="/reservation.html">来店予約</a></li>
    </ul>
    <!--************************ Section contact start ************************-->
    <section class="p-contact1">
        <h3 class="c-title1">[ 各種お問い合わせ ]</h3>
        <div class="c-text1">
            <p>ご入力いただく個人情報は、お問い合わせの回答を目的として使用します。<br>
                ご連絡等に必要な個人情報報取り扱いに関しましては「プライバシーポリシー」をご覧下さい。<br>
                [必須]印のついた項目は、必ずご入力ください。
            </p>
        </div>
    </section>
    <!--************************ Section contact1 end ************************-->
    <!--************************ contact form start ************************-->
    <form id="mailform" method="POST" class="c-form1">
        <div class="c-form1__col">
            <label class="c-form1__col--left">お名前</label>
            <input class="c-form1__w01" type="text" name="お名前(必須)">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">ご住所</label>
            <input class="c-form1__w01" type="text" name="ご住所">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">お電話番号</label>
            <input class="c-form1__w01" type="text" name="お電話番号(必須)">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">FAX番号</label>
            <input class="c-form1__w01" type="text" name="FAX番号">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">メールアドレス</label>
            <input class="c-form1__w01" type="text" name="メールアドレス(必須)">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">ご職業</label>
            <select name="ご職業(必須)" id="ご職業(必須)">
                <option value="会社員">会社員</option>
                <option value="会社役員">会社役員</option>
                <option value="公務員">公務員</option>
                <option value="自営業">自営業</option>
                <option value="その他専⾨門職">その他専⾨門職</option>
                <option value="パート・アルバイト">パート・アルバイト</option>
                <option value="主婦・主夫">主婦・主夫</option>
                <option value="学⽣">学⽣</option>
                <option value="その他">その他</option>
            </select>
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">ご希望の連絡方法</label>
            <div class="c-form1__col--right">
                <label class="c-form1__checker">メール
                    <input type="radio" name="性別(必須)" value="メール">
                    <span class="radioCheck"></span>
                </label>
                <div class="c-form1-time">
                    <label class="c-form1__checker">お電話(ご希望の時間帯：
                        <input type="radio" name="性別(必須)" value="お電話">
                        <span class="radioCheck"></span>
                    </label>
                    <input class="c-form1__w03" type="text" name="メールアドレス(必須)"> <small>時頃)</small>
                </div>
            </div>
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">お問い合わせ内容</label>
            <textarea name="お問い合わせ内容(必須)"></textarea>
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">件名</label>
            <div class="c-form1__col--right">
                <label class="c-form1__checker">売却希望の方
                    <input type="radio" name="性別(必須)" value="売却希望の方">
                    <span class="radioCheck"></span>
                </label>
                <label class="c-form1__checker">購入希望の方
                    <input type="radio" name="性別(必須)" value="購入希望の方">
                    <span class="radioCheck"></span>
                </label> 
                <label class="c-form1__checker">仲介希望の方
                    <input type="radio" name="性別(必須)" value="仲介希望の方">
                    <span class="radioCheck"></span>
                </label>
                <label class="c-form1__checker">その他お問い合わせ
                    <input type="radio" name="性別(必須)" value="その他お問い合わせ">
                    <span class="radioCheck"></span>
                </label>
            </div>
        </div>
            <div class="c-form1__col">
            <label class="c-form1__col--left">物件種別</label>
            <select name="物件種別(必須)" id="物件種別(必須)">
                <option value="マンション">マンション</option>
                <option value="中古住宅">中古住宅</option>
                <option value="新築分譲住宅">新築分譲住宅</option>
                <option value="店舗・事務所">店舗・事務所</option>
                <option value="住宅用地">住宅用地</option>
                <option value="事業用地">事業用地</option>
            </select>
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">地域選択</label>
            <select name="地域選択(必須)" id="地域選択(必須)">
                <option value="刈谷市">刈谷市</option>
                <option value="安城市">安城市</option>
                <option value="知立市">知立市</option>
                <option value="高浜市">高浜市</option>
                <option value="碧南市">碧南市</option>
                <option value="その他">その他</option>
            </select>
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">希望価格</label>
            <select name="希望価格(必須)" id="希望価格(必須)">
                <option value="1,000万円以下">1,000万円以下</option>
                <option value="1,000万円-1,500万円まで">1,000万円-1,500万円まで</option>
                <option value="1,500万円-2,000万円まで">1,500万円-2,000万円まで</option>
                <option value="2,000万円-2,500万円まで">2,000万円-2,500万円まで</option>
                <option value="2,500万円-3,000万円まで">2,500万円-3,000万円まで</option>
                <option value="3,000万円-3,500万円まで">3,000万円-3,500万円まで</option>
                <option value="3,500万円-4,000万円まで">3,500万円-4,000万円まで</option>
                <option value="4,000万円-4,500万円まで">4,000万円-4,500万円まで</option>
                <option value="4,500万円-5,000万円まで">4,500万円-5,000万円まで</option>
                <option value="5,000万円-5,500万円まで">5,000万円-5,500万円まで</option>
                <option value="5,500万円-6,000万円まで">5,500万円-6,000万円まで</option>
                <option value="6,000万円-6,500万円まで">6,000万円-6,500万円まで</option>
                <option value="6,500万円-7,000万円まで">6,500万円-7,000万円まで</option>
            </select>
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left"></label>
            <div class="c-form1__col--right">
                <button type="submit" value="メールを送信する">送　信</button>
            </div>
        </div>
    </form>
    <!--************************ contact form end ************************-->
</div>
