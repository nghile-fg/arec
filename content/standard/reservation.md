---json
{
    "title": "あいち不動産コンサルタンツ",
    "description": "土地、中古住宅、新築住宅、中古マンションなどの不動産売買仲介情報。 不動産有効活用、各種権利調整、相続対策関連、不動産経営分野などの不動産コンサルティング。 
  ライフプラン作成、住宅ローンアドバイス、資金計画相談など資産運用の不動産FP相談。 賃貸マンション、貸家、テナント（貸し店舗・貸し事務所）などの賃貸仲介情報",
    "keywords":"あいち不動産コンサルタンツ,不動産コンサルティング,FP,ファイナンシャルプランナー, 不動産, 三河, 刈谷, 安城, 知立, 賃貸, 物件, 仲介, 売買, 土地, 貸店舗, 貸事務所, 
  売り地, 中古, マンション",
    "slug": "",
    "layout": "contact.html",
    "tag": [],
    "date": "2017-04-18T04:38:58.000Z",
    "publishDate": "2017-04-18T04:38:58.000Z",
    "draft": false,
    "__content__": ""
}
---

<div class="p-contact">
    <!--************************ MainBlock start ************************-->
    <div class="c-mainBlock">
        <div class="c-mainBlock__img"><img src="./assets/img/contact/index_contact.png" alt="会社案内"></div>
    </div>
    <!--************************ MainBlock end ************************-->
    <ul class="c-navi1">
        <li><a href="/contact_all.html">各種お問い合わせ</a></li>
        <li><a href="/reservation.html">来店予約</a></li>
    </ul>
    <!--************************ Section contact start ************************-->
    <section class="p-contact1">
        <h3 class="c-title1">[ 来店予約 ]</h3>
        <div class="c-text1">
            <p>ご入力いただく個人情報は、お問い合わせの回答を目的として使用します。<br>
                ご連絡等に必要な個人情報報取り扱いに関しましては「プライバシーポリシー」をご覧下さい。<br>
                [必須]印のついた項目は、必ずご入力ください。
            </p>
        </div>
    </section>
    <!--************************ Section contact1 end ************************-->
    <!--************************ contact form start ************************-->
    <form id="mailform" method="POST" class="c-form1">
        <div class="c-form1__col">
            <label class="c-form1__col--left">お名前</label>
            <input class="c-form1__w01" type="text" name="お名前(必須)">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">ご住所</label>
            <input class="c-form1__w01" type="text" name="ご住所">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">お電話番号</label>
            <input class="c-form1__w01" type="text" name="お電話番号(必須)">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">FAX番号</label>
            <input class="c-form1__w01" type="text" name="FAX番号">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">メールアドレス</label>
            <input class="c-form1__w01" type="text" name="メールアドレス(必須)">
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">ご職業</label>
            <select name="ご職業(必須)" id="ご職業(必須)">
                <option value="会社員">会社員</option>
                <option value="会社役員">会社役員</option>
                <option value="公務員">公務員</option>
                <option value="自営業">自営業</option>
                <option value="その他専⾨門職">その他専⾨門職</option>
                <option value="パート・アルバイト">パート・アルバイト</option>
                <option value="主婦・主夫">主婦・主夫</option>
                <option value="学⽣">学⽣</option>
                <option value="その他">その他</option>
            </select>
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">ご希望の連絡方法</label>
            <div class="c-form1__col--right">
                <label class="c-form1__checker">メール
                    <input type="radio" name="性別(必須)" value="メール">
                    <span class="radioCheck"></span>
                </label>
                <div class="c-form1-time">
                    <label class="c-form1__checker">お電話(ご希望の時間帯：
                        <input type="radio" name="性別(必須)" value="お電話">
                        <span class="radioCheck"></span>
                    </label>
                    <input class="c-form1__w03" type="text" name="メールアドレス(必須)"> <small>時頃)</small>
                </div>
            </div>
        </div>
        <div class="l-form">
            <div class="l-form__left">
                <label class="c-form1__col--left">ご来店日時(第1希望)</label>
            </div>
            <div class="l-form__right">
                <div class="c-form1__col">
                    <label class="c-form1__col--left">ご来店日</label>
                    <input class="c-form1__w02" type="date" name="ご来店日(必須)">
                </div>
                <div class="c-form1__col">
                    <label class="c-form1__col--left">ご来店時間</label>
                    <select name="物件種別(必須)" id="物件種別(必須)">
                        <option value="9:00〜">9:00〜</option>
                        <option value="10:00〜">10:00〜</option>
                        <option value="11:00〜">11:00〜</option>
                        <option value="12:00〜">12:00〜</option>
                        <option value="13:00〜">13:00〜</option>
                        <option value="14:00〜">14:00〜</option>
                        <option value="15:00〜">15:00〜</option>
                        <option value="16:00〜">16:00〜</option>
                        <option value="17:00〜">17:00〜</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="l-form">
            <div class="l-form__left">
                <label class="c-form1__col--left">ご来店日時(第2希望)</label>
            </div>
            <div class="l-form__right">
                <div class="c-form1__col">
                    <label class="c-form1__col--left">ご来店日</label>
                    <input class="c-form1__w02" type="date" name="ご来店日(必須)">
                </div>
                <div class="c-form1__col">
                    <label class="c-form1__col--left">ご来店時間</label>
                    <select name="物件種別(必須)" id="物件種別(必須)">
                        <option value="9:00〜">9:00〜</option>
                        <option value="10:00〜">10:00〜</option>
                        <option value="11:00〜">11:00〜</option>
                        <option value="12:00〜">12:00〜</option>
                        <option value="13:00〜">13:00〜</option>
                        <option value="14:00〜">14:00〜</option>
                        <option value="15:00〜">15:00〜</option>
                        <option value="16:00〜">16:00〜</option>
                        <option value="17:00〜">17:00〜</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left">件名</label>
            <div class="c-form1__col--right">
                <label class="c-form1__checker">売却希望の方
                    <input type="radio" name="性別(必須)" value="売却希望の方">
                    <span class="radioCheck"></span>
                </label>
                <label class="c-form1__checker">購入希望の方
                    <input type="radio" name="性別(必須)" value="購入希望の方">
                    <span class="radioCheck"></span>
                </label> 
                <label class="c-form1__checker">仲介希望の方
                    <input type="radio" name="性別(必須)" value="仲介希望の方">
                    <span class="radioCheck"></span>
                </label>
                <label class="c-form1__checker">その他お問い合わせ
                    <input type="radio" name="性別(必須)" value="その他お問い合わせ">
                    <span class="radioCheck"></span>
                </label>
            </div>
        </div>
        <div class="c-form1__col">
            <label class="c-form1__col--left"></label>
            <div class="c-form1__col--right">
                <button type="submit" value="メールを送信する">送　信</button>
            </div>
        </div>
    </form>
    <!--************************ contact form end ************************-->
</div>