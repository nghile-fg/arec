/*===================================================
Viewport width fit for Tablet
===================================================*/
var _ua = (function(u) {
  return {
    Tablet: (u.indexOf("windows") != -1 && u.indexOf("touch") != -1 && u.indexOf("tablet pc") == -1) || u.indexOf("ipad") != -1 || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1) || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1) || u.indexOf("kindle") != -1 || u.indexOf("silk") != -1 || u.indexOf("playbook") != -1,
    Mobile: (u.indexOf("windows") != -1 && u.indexOf("phone") != -1) || u.indexOf("iphone") != -1 || u.indexOf("ipod") != -1 || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1) || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1) || u.indexOf("blackberry") != -1
  }
}
)(window.navigator.userAgent.toLowerCase());

if (_ua.Tablet) {
  $("meta[name='viewport']").attr('content', 'width=1100');
}

/*-----------------------------------------------------------------------------------*/
/* Start Slider index by slick
/*-----------------------------------------------------------------------------------*/

$('#sliderIndex').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  infinite: true,
  dots: true,
  autoplay: true,
  autoplaySpeed: 2500,
  centerMode: true,
  centerPadding: 0,
  arrows: true,
  nextArrow: '<img class="right-arrow" src="./assets/img/index/iconright.png"/>',
  prevArrow: '<img class="left-arrow" src="./assets/img/index/iconsleft.png"/>',
  responsive: [
  {
    breakpoint: 768,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
  ]
});
$('#sliderIndex').slick('slickPause');
// End Slider index by slick
/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/* Start Effect for menu top
/*-----------------------------------------------------------------------------------*/
var menubar = document.getElementById('menuBar');
var gnavi = document.getElementById('gNavi');
var h_scroll;
var e_header = document.getElementById('headerEffect');
menubar.addEventListener('click', barActive, false);
function barActive(){
  this.classList.toggle('is-bar');
  gnavi.classList.remove('is-righshow');
  var findClass = this.classList.contains("is-bar");
  if(findClass){
    gnavi.classList.add('is-righshow');
    document.body.classList.add('fixedBody');
    document.body.style.top = '-'+h_scroll + 'px';
    e_header.classList.add('fixed01');
  } else {
    document.body.classList.remove('fixedBody');
    var strBody = document.body.style.top;
    var lstrBody = strBody.length;
    var bodyH = strBody.substring(1, lstrBody - 2);
    e_header.classList.remove('fixed01');
    $('html, body').stop().animate({
        scrollTop: Math.floor(bodyH)
    }, 0);
  }
}

var cloNavi = document.getElementById('closeNavi');
cloNavi.addEventListener('click', closeNavi, false);
function closeNavi(){
  menubar.classList.remove('is-bar');
  gnavi.classList.remove('is-righshow');
  document.body.classList.remove('fixedBody');
  var strBody = document.body.style.top;
  var lstrBody = strBody.length;
  var bodyH = strBody.substring(1, lstrBody - 2);
  e_header.classList.remove('fixed01');
  $('html, body').stop().animate({
      scrollTop: Math.floor(bodyH)
  }, 0);
}

/* effect Menu sp
/*-----------------------------------------------------------------------------------*/

var subMneu = document.querySelectorAll('.c-gnavi__SP li');
for(var i=0; i<subMneu.length;i++){
  subMneu[i].addEventListener('click', showSubMenu);
}
function showSubMenu(){
  this.classList.toggle('is-submenu');
}

// End Effect for menu top
/*-----------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------------*/
/* Start fixed header when scroll page
/*-----------------------------------------------------------------------------------*/
  window.addEventListener('scroll', function() {
    if ( window.pageYOffset > 0) {
      e_header.classList.add('fixed');
    } else {
      e_header.classList.remove('fixed');
    }
  });

// End fixed header when scroll page
/*-----------------------------------------------------------------------------------*/

// Start Fade up element
/*-----------------------------------------------------------------------------------*/
var e_fadeup = document.getElementsByClassName('l-upToo');
function activeFadeup(value) {
  var b_w = window.outerHeight + window.pageYOffset; // vị trí thấp nhất khi scorll
  for (var i = 0; i < value.length; i++) {
    if($(window).width() > 767) {
      var h_elm = value[i].offsetTop + value[i].clientHeight;
      var e_height = value[i].clientHeight + h_elm;
    }
    else{
      var h_elm = value[i].offsetTop + value[i].clientHeight - 260;
      var e_height = value[i].clientHeight + h_elm + 150;
    }
    if((e_height >= b_w) && (h_elm <= b_w) || (b_w + 150 > e_height) ){
      value[i].classList.add('is-fadeup');
      value[i].classList.remove('l-upToo');
      $('#sliderIndex').slick('slickPlay');
    }
  };
}
window.onscroll = function() {
  h_scroll = window.pageYOffset;
  activeFadeup(e_fadeup)};
window.onload = function() {activeFadeup(e_fadeup)};

/*-----------------------------------------------------------------------------------*/
/* Start Slider Site SP
/*-----------------------------------------------------------------------------------*/
$('#slideSite').slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  infinite: true,
  dots: false,
  autoplay: true,
  autoplaySpeed: 3000,
  centerMode: true,
  centerPadding: 0,
  arrows: false,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        centerMode: true,
        nextArrow: '<img class="right-arrow" src="./assets/img/index/iconright.png"/>',
        prevArrow: '<img class="left-arrow" src="./assets/img/index/iconsleft.png"/>',
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          arrows: true,
          nextArrow: '<img class="right-arrow" src="./assets/img/index/iconright.png"/>',
          prevArrow: '<img class="left-arrow" src="./assets/img/index/iconsleft.png"/>',
        }
      },
    ]
});

var subFt = document.querySelectorAll('.c-gNavift li');
for(var i=0; i<subFt.length;i++){
  subFt[i].addEventListener('click', showSubFooter);
}
function showSubFooter(){
  this.classList.toggle('is-iconft');
  this.firstChild.nextElementSibling.classList.toggle('is-showft');
}

/* Search page header menu */
$(document).ready(function(){
  var $searchTrigger = $('[data-ic-class="search-trigger"]'),
      $searchInput = $('[data-ic-class="search-input"]'),
      $searchClear = $('[data-ic-class="search-clear"]');
  $searchTrigger.click(function(){
    var $this = $('[data-ic-class="search-trigger"]');
    $this.addClass('active');
    $searchInput.focus();
  });
  $searchInput.blur(function(){
    if($searchInput.val().length > 0){
      return false;
    } else {
      $searchTrigger.removeClass('active');
    }
  });
  $searchClear.click(function(){
    $searchInput.val('');
  });
  $searchInput.focus(function(){
    $searchTrigger.addClass('active');
  });
});

var iframe = document.querySelectorAll('#iframe .item');
var iframeClick = document.querySelectorAll('#iframeClick a');
for(var i=0; i<iframeClick.length;i++){
  iframeClick[i].addEventListener('click', activeIframe);
}
function activeIframe(e) {
  e.preventDefault();
  for(var i=0; i<iframe.length;i++){
    iframe[i].classList.remove('show-ifr');
  }
  iframe[this.getAttribute('data-iframe')].classList.add('show-ifr');
}

// Slider page skill
/* <!--************************ Effect sroll to element start************************--> */
$('a[href^="#"]').click(function(e){
  e.preventDefault();
  var href= $(this).attr("href");
  var target = $(href == "#" || href == "" ? 'html' : href);
  var position = target.offset().top - 60;
  $("html, body").animate({scrollTop:position}, 'slow');
});